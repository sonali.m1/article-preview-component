function counterFactory() {
  var value = 30;
  function increment() {
    return ++value;
  }
  function dicrement() {
    return --value;
  }
  return { increment, dicrement };
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
}
console.log(counterFactory().increment());
console.log(counterFactory().dicrement());
// ---------------------------------------------------------------------------
function cb() {
  console.log("Callback invoked");
}
function limitFunctionCallCount(cb, n) {
  let counter = n;
  function invoke() {
    counter-- > 0 ? cb() : console.log(`Callback already called ${n}-times`);
  }
  return { invoke };
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
}
var closure = limitFunctionCallCount(cb, 2);
closure.invoke();
closure.invoke();
closure.invoke();
// ----------------------------------------------------------------------------------------------
cb = (x) => `Good! argument ${x} was no used!`;

function cacheFunction(cb) {
  // Should return a funciton that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
  const cache = new Set();
  function again(param) {
    if (!cache.has(param)) {
      cache.add(param);
      return cb(param);
    } else {
      return cache;
    }
  }
  return { again };
}
let argumentUsed = cacheFunction(cb);
console.log(argumentUsed.again("x"));
console.log(argumentUsed.again("x"));
console.log(argumentUsed.again("y"));
console.log(argumentUsed.again("x"));
console.log(argumentUsed.again("y"));
