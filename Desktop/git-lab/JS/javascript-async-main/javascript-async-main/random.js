function fetchRandomNumbers() {
  return new Promise((resolve, reject) => {
    console.log("Fetching number...");
    setTimeout(() => {
      let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
      console.log("Received random number:", randomNum);
      resolve(randomNum);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

function fetchRandomString() {
  return new Promise((resolve, reject) => {
    console.log("Fetching string...");
    setTimeout(() => {
      let result = "";
      let characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      let charactersLength = characters.length;
      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      console.log("Received random string:", result);
      resolve(result);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

fetchRandomNumbers((randomNum) => console.log(randomNum));
fetchRandomString((randomStr) => console.log(randomStr));

// -------------------------------------------------1st Task-------------------------------------------------------------

// **Task 1**: Right now, the function fetchRandomNumbers can be used by passing a callback,
// Your task is to promisfy this function so that the following can be done:

// fetchRandomNumbers().then((randomNum) => {
//   console.log(randomNum);
// });
function task1() {
  fetchRandomNumbers().then((randomNum) => {
    console.log(randomNum);
  });
}
task1();

// Similarlly, do the same for the function fetchRandomString

// -------------------------------------------------2nd Task-------------------------------------------------------------
// **Task 2**: Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable
// -> add it to the same sum variable and print the sum variable.
function task2() {
  var value = 0;
  fetchRandomNumbers().then((randomNum) => (value = randomNum));
  fetchRandomNumbers().then((randomNum) => {
    value += randomNum;
    setTimeout(() => {
      console.log(value);
    }, 2000);
  });
}
task2();

// -------------------------------------------------3rd Task-------------------------------------------------------------
// **Task 3**: Fetch a random number and a random string simultaneously, concatenate their
// and print the concatenated string
function task3() {
  var value = "";
  fetchRandomNumbers().then((randomNum) => (value = randomNum));
  fetchRandomString().then((randomStr) => {
    value += randomStr;
    setTimeout(() => {
      console.log(value);
    }, 2000);
  });
}
task3();
// -------------------------------------------------4th Task-------------------------------------------------------------
// **Task 4**: Fetch 10 random numbers simultaneously -> and print their sum.
// # Async await
function task4() {
  var value = 0;
  for (let i = 0; i < 0; i++) {
    fetchRandomNumbers().then((randomNum) => (value += randomNum));
  }
  setTimeout(() => {
    console.log(value);
  }, 5000);
}
task4();
